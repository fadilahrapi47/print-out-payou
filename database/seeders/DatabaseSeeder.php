<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Template;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Template::create([
            'template_name' => 'Cash Disbursement Half',
            'template_content' => '<table id="table-1" style="width: 100%; border-collapse: collapse; background-color: #54acd2; color: #ffffff; font-family: Arial,Helvetica,sans-serif;">
            <tbody>
            <tr>
            <td style="padding: 20px; font-size: 24px; width: 38%;"><strong>CASH DISBURSEMENT</strong></td>
            <td style="padding: 20px; text-align: right; font-size: 12px; vertical-align: top; width: 62%;"><strong>{company.name}</strong><br />{company.address}<br />{company.city}, {company.country_name}, {company.postal_code}<br />{company.phone} {company.email}</td>
            </tr>
            </tbody>
            </table>
            <div style="padding: 0px 20px 20px 20px; font-family: Arial,Helvetica,sans-serif;">
            <table id="table-2" style="width: 100%; border-collapse: collapse; font-size: 14px;">
            <tbody>
            <tr>
            <td style="padding: 8px 0 8px 0; width: 75%;"><span style="color: #475577;">To :</span><br /><strong>{contact.name}</strong><br /><br /><span style="color: #475577;">To :</span><br /><strong>{cash.account.name}</strong></td>
            <td style="padding: 8px 0 8px 0; width: 25%;"><span style="color: #475577;">Reference Number :</span><br /><strong>{number}</strong><br /><br /><span style="color: #475577;">Date :</span><br /><strong>{date}</strong></td>
            </tr>
            <tr>
            <td colspan="2"> </td>
            </tr>
            <tr>
            <td style="padding: 8px 0 8px 0;" colspan="2"><span style="color: #475577;">Description :</span><br />{description}</td>
            </tr>
            <tr>
            <td colspan="2"> </td>
            </tr>
            </tbody>
            </table>
            <table id="table-3" style="width: 100%; border-collapse: collapse; font-size: 12px;">
            <thead>
            <tr>
            <th style="width: 20%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Account Code</th>
            <th style="width: 30%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Account Name</th>
            <th style="width: 30%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Notes</th>
            <th style="width: 20%; padding: 8px; background-color: #54acd2; color: #fff; text-align: right;">Amount</th>
            </tr>
            </thead>
            <tbody class="striped"><!--line_items-->
            <tr>
            <td style="padding: 8px;">{item.account.code}</td>
            <td style="padding: 8px;">{item.account.name}</td>
            <td style="padding: 8px;">{item.note}</td>
            <td style="padding: 8px; text-align: right;">{item.currency.symbol}{item.amount_origin}</td>
            </tr>
            <!--line_items-->
            <tr>
            <td style="padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"><strong>Total</strong></td>
            <td style="padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"> </td>
            <td style="padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"> </td>
            <td style="padding: 8px; background-color: #54acd2; color: #fff; text-align: right;"><strong>{cash.currency.symbol}{cash.amount_origin}</strong></td>
            </tr>
            </tbody>
            </table>
            <table id="table-4" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-size: 12px;">
            <tbody>
            <tr>
            <td style="padding: 8px;"> </td>
            <td> </td>
            </tr>
            </tbody>
            </table>
            </div>'
        ]);
        Template::create([
            'template_name' => 'Cash Disbursement Plain',
            'template_content' => '<table id="table-1" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif;">
            <tbody>
            <tr>
            <td style="border-bottom: 1px solid black; padding: 20px; font-size: 24px; width: 38%;"><strong>CASH DISBURSEMENT</strong></td>
            <td style="border-bottom: 1px solid black; padding: 20px; text-align: right; font-size: 12px; vertical-align: top; width: 62%;"><strong>{company.name}</strong><br />{company.address}<br />{company.city}, {company.country_name}, {company.postal_code}<br />{company.phone} {company.email}</td>
            </tr>
            </tbody>
            </table>
            <div style="padding: 0px 20px 20px 20px; font-family: Arial,Helvetica,sans-serif;">
            <table id="table-2" style="width: 100%; border-collapse: collapse; font-size: 14px;">
            <tbody>
            <tr>
            <td style="padding: 8px 0 8px 0; width: 75%;"><span style="color: #475577;">To :</span><br /><strong>{contact.name}</strong><br /><br /><span style="color: #475577;">To :</span><br /><strong>{cash.account.name}</strong></td>
            <td style="padding: 8px 0 8px 0; width: 25%;"><span style="color: #475577;">Reference Number :</span><br /><strong>{number}</strong><br /><br /><span style="color: #475577;">Date :</span><br /><strong>{date}</strong></td>
            </tr>
            <tr>
            <td colspan="2"> </td>
            </tr>
            <tr>
            <td style="padding: 8px 0 8px 0;" colspan="2"><span style="color: #475577;">Description :</span><br />{description}</td>
            </tr>
            <tr>
            <td colspan="2"> </td>
            </tr>
            </tbody>
            </table>
            <table id="table-3" style="width: 100%; border-collapse: collapse; font-size: 12px;">
            <thead>
            <tr>
            <th style="border: 1px solid black; width: 20%; padding: 8px; text-align: left;">Account Code</th>
            <th style="border: 1px solid black; width: 30%; padding: 8px; text-align: left;">Account Name</th>
            <th style="border: 1px solid black; width: 30%; padding: 8px; text-align: left;">Notes</th>
            <th style="border: 1px solid black; width: 20%; padding: 8px; text-align: right;">Amount</th>
            </tr>
            </thead>
            <tbody><!--line_items-->
            <tr>
            <td style="border: 1px solid black; padding: 8px;">{item.account.code}</td>
            <td style="border: 1px solid black; padding: 8px;">{item.account.name}</td>
            <td style="border: 1px solid black; padding: 8px;">{item.note}</td>
            <td style="border: 1px solid black; padding: 8px; text-align: right;">{item.currency.symbol}{item.amount_origin}</td>
            </tr>
            <!--line_items-->
            <tr>
            <td style="border: 1px solid black; padding: 8px; text-align: left;" colspan="3"><strong>Total</strong></td>
            <td style="border: 1px solid black; padding: 8px; text-align: right;"><strong>{cash.currency.symbol}{cash.amount_origin}</strong></td>
            </tr>
            </tbody>
            </table>
            <table id="table-4" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-size: 12px;">
            <tbody>
            <tr>
            <td style="padding: 8px;"> </td>
            <td> </td>
            </tr>
            </tbody>
            </table>
            </div>
            <table id="table-5" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-size: 12px;">
            <tbody>
            <tr>
            <td style="padding: 20px; width: 35%; text-align: center; vertical-align: top;">created by:<br /><br /><br /><br /><br /><br /><br />__________________<br />{created.user.name}</td>
            <td style="padding: 20px; width: 35%; text-align: center; vertical-align: top;">received by:<br /><br /><br /><br /><br /><br /><br />__________________</td>
            </tr>
            </tbody>
            </table>'
        ]);
        Template::create([
            'template_name' => 'Cash Disbursement Classic',
            'template_content' => '<table id="table-1" style="width: 100%; border-collapse: collapse; color: #54acd2; background-color: #ffffff; font-family: Arial,Helvetica,sans-serif;">
            <tbody>
            <tr>
            <td style="border-bottom: 1px solid #54acd2; padding: 20px; font-size: 24px; width: 38%;"><strong>CASH DISBURSEMENT</strong></td>
            <td style="border-bottom: 1px solid #54acd2; padding: 20px; text-align: right; font-size: 12px; vertical-align: top; width: 62%;"><strong>{company.name}</strong><br />{company.address} <br />{company.city}, {company.country_name}, {company.postal_code} <br />{company.phone} {company.email}</td>
            </tr>
            </tbody>
            </table>
            <div style="padding: 0px 20px 20px 20px; font-family: Arial,Helvetica,sans-serif;">
            <table id="table-2" style="width: 100%; border-collapse: collapse; font-size: 14px;">
            <tbody>
            <tr>
            <td style="padding: 8px 0 8px 0; width: 75%;"><span style="color: #475577;">To :</span><br /><strong>{contact.name} <br /><br /></strong><span style="color: #475577;">Departement:</span><strong><br />{department.name} <br /><br /><span style="color: #475577;">Project:</span><br /></strong><strong>{project.name}</strong></td>
            <td style="padding: 8px 0 8px 0; width: 25%;"><span style="color: #475577;">Reference Number :</span><br /><strong>{number}</strong><br /><br /><span style="color: #475577;">Date :</span><br /><strong>{date} <br /><br /><br /></strong></td>
            </tr>
            <tr>
            <td style="width: 100%;" colspan="2"> </td>
            </tr>
            <tr>
            <td style="padding: 8px 0px; width: 100%;" colspan="2"><span style="color: #475577;">Description :</span><br />{description}</td>
            </tr>
            <tr style="height: 18px;">
            <td style="height: 18px; width: 100%; text-align: right;" colspan="2"><font size="1"><em>Ex.Rate 1 {cash.currency.code} = {company_currency.symbol} {cash.exchange_rate}</em></font></td>
            </tr>
            </tbody>
            </table>
            <table id="table-3" style="width: 100%; border-collapse: collapse; font-size: 12px;">
            <thead>
            <tr>
            <th style="border: 1px solid #54acd2; width: 20%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Account Code</th>
            <th style="border: 1px solid #54acd2; width: 30%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Account Name</th>
            <th style="border: 1px solid #54acd2; width: 30%; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;">Notes</th>
            <th style="border: 1px solid #54acd2; width: 20%; padding: 8px; background-color: #54acd2; color: #fff; text-align: right;">Amount</th>
            </tr>
            </thead>
            <tbody><!--line_items-->
            <tr>
            <td style="border: 1px solid #54acd2; padding: 8px;">{item.account.code}</td>
            <td style="border: 1px solid #54acd2; padding: 8px;">{item.account.name}</td>
            <td style="border: 1px solid #54acd2; padding: 8px;">{item.note}</td>
            <td style="border: 1px solid #54acd2; padding: 8px; text-align: right;">{item.currency.symbol}{item.amount_origin}</td>
            </tr>
            <!--line_items-->
            <tr>
            <td style="border: 1px solid #54acd2; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"><strong>Total</strong></td>
            <td style="border: 1px solid #54acd2; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"> </td>
            <td style="border: 1px solid #54acd2; padding: 8px; background-color: #54acd2; color: #fff; text-align: left;"> </td>
            <td style="border: 1px solid #54acd2; padding: 8px; background-color: #54acd2; color: #fff; text-align: right;"><strong>{cash.currency.symbol}{cash.amount_origin}</strong></td>
            </tr>
            </tbody>
            </table>
            <table id="table-4" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-size: 12px;">
            <tbody>
            <tr>
            <td style="padding: 8px;"> </td>
            <td> </td>
            </tr>
            </tbody>
            </table>
            </div>
            <table id="table-5" style="width: 100%; border-collapse: collapse; font-family: Arial,Helvetica,sans-serif; font-size: 12px;">
            <tbody>
            <tr>
            <td style="padding: 20px; width: 35%; text-align: center; vertical-align: top;">created by: <br /><br /><br /><br /><br /><br /><br />__________________ <br />{created.user.name}</td>
            <td style="padding: 20px; width: 35%; text-align: center; vertical-align: top;">received by: <br /><br /><br /><br /><br /><br /><br />__________________</td>
            </tr>
            </tbody>
            </table>'
        ]);
    }
}
