@extends('layouts.main')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 w-100 mt-5 mb-2">
        <div class="d-block py-3 mb-1">
          <div class="d-flex">
            <button type="button" class="btn btn-outline-danger me-2" data-bs-toggle="modal" data-bs-target="#backtohome">
              Back to Home
            </button>
            <div class="modal fade" id="backtohome" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 style="font-weight: bold; color: black">Are you sure?</h1>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">No</button>
                    <a href="/" class="btn btn-success">Yes </a>
                  </div>
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-success me-auto" data-bs-toggle="modal" data-bs-target="#saveTemplate">Save Template</button>
            <div class="modal fade" id="saveTemplate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Create New Template</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form class="form-floating" id="myform" method="POST" action="{{ route('store') }}">
                      @csrf
                      <input type="text" class="form-control" name="title">
                      <label for="floatingInputValue">Template Name</label>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                    <button type="submit" name="submit" form="myform" class="btn btn-success" disabled>Create Template</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          <!-- TinyMCE Text Editor Area -->
          <div class="d-block bg-white">
            <textarea type="text" id="tinymceditor" name="body" form="myform">
            </textarea>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    const inputForm = document.querySelector('input[name="title"]');
    const btnSubmit = document.querySelector('button[name="submit"]');
    inputForm.addEventListener("keyup", (e) => {
      if (!(e.target.value == "")) {
        btnSubmit.removeAttribute("disabled");
      } else {
        btnSubmit.setAttribute("disabled", "");
      }
    });
  </script>
@endsection
