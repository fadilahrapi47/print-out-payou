<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\Template;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/create-new-template', function () {
    return view('create');
});

Route::get('/update-delete-template/{id}', function ($id) {
    $template = Template::find($id);
    return view('update', compact('template'));
});

Route::get('/', [Controller::class, 'templates'])->name('templates');
Route::get('/templates-list', [Controller::class, 'templatesList'])->name('templates-list');
Route::get('/search-template', [Controller::class, 'searchTemplate'])->name('search-template');
Route::post('/create', [Controller::class, 'store'])->name('store');
Route::post('/update', [Controller::class, 'update'])->name('update');
Route::post('/delete', [Controller::class, 'delete'])->name('delete');
