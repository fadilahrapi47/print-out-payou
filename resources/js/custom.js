const ajax = new XMLHttpRequest();
const wrapper = document.querySelector(".wrapper");
const updateBtn = document.querySelector("#update-btn");
const selectBtn = wrapper.querySelector(".select-btn");
const searchInp = wrapper.querySelector(".search");
const options = wrapper.querySelector(".options");
const templateContent = document.getElementById("template-content");

ajax.onreadystatechange = () => {
    if (ajax.readyState == 4 && ajax.status == 200) {
        const templatesList = JSON.parse(ajax.responseText);
        const selectedOption =
            wrapper.querySelector(".select-btn span").innerText;
        addTemplate("Cash Disbursement Half", templatesList);
        bindingTemplate(selectedOption);
    } else if (ajax.status == 404) {
        options.innerHTML = `<p style="margin-top: 10px; padding-left: 15px;">Error ${ajax.status} Data not found.</p>`
    }
};

function bindingTemplate(selectedTemplate) {
    ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 200) {
            const myTemplates = JSON.parse(ajax.responseText);
            myTemplates.map((data) => {
                templateContent.innerHTML = data.template_content;
            });
        }
    };
    ajax.open("GET", "/search-template?search_string=" + selectedTemplate);
    ajax.send();
}

function myTemplates(lists, templates) {
    lists.forEach((list) => {
        list.addEventListener("click", (e) => {
            updateName(e.target, templates);
        });
    });
}

function addTemplate(selectedTemplate, templates) {
    options.innerHTML = "";
    templates.forEach((template) => {
        let isSelected =
            template.template_name == selectedTemplate
                ? "selected-template"
                : "";
        let templates = `<li id="list-item" class="${isSelected}">${template.template_name}</li>`;
        options.insertAdjacentHTML("beforeend", templates);
    });
    const listTemplates = wrapper.querySelectorAll("#list-item");
    myTemplates(listTemplates, templates);
}

function updateName(selectedMenu, templates) {
    searchInp.childNodes[3].value = "";
    addTemplate(selectedMenu.innerText, templates);
    wrapper.classList.remove("active");
    selectBtn.firstElementChild.innerText = selectedMenu.innerText;
    bindingTemplate(selectBtn.firstElementChild.innerText);
}

searchInp.addEventListener("keyup", () => {
    let search_string = searchInp.childNodes[3].value;
    ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 200) {
            const templateObject = JSON.parse(ajax.responseText);
            if (templateObject.status == "nothing_found") {
                options.innerHTML = `<p style="margin-top: 10px; padding-left: 15px;">Template not found.</p>`;
            } else {
                let arr = [];
                arr = templateObject
                    .map((data) => {
                        let isSelected =
                            data.template_name ==
                            selectBtn.firstElementChild.innerText
                                ? "selected-template"
                                : "";
                        return `<li id="list-item" class="${isSelected}">${data.template_name}</li>`;
                    })
                    .join("");
                options.innerHTML = arr;
                const listTemplates = wrapper.querySelectorAll("#list-item");
                listTemplates.forEach((list) => {
                    list.addEventListener("click", (e) => {
                        // callback hell
                        ajax.onreadystatechange = () => {
                            if (ajax.readyState == 4 && ajax.status == 200) {
                                const allTemplates = JSON.parse(
                                    ajax.responseText
                                );

                                updateName(e.target, allTemplates);
                            }
                        };
                        ajax.open("GET", "/templates-list");
                        ajax.send();
                    });
                });
            }
        }
    };
    ajax.open("GET", "/search-template?search_string=" + search_string);
    ajax.send();
});

selectBtn.addEventListener("click", () => wrapper.classList.toggle("active"));

updateBtn.addEventListener("click", () => {
    const getTemplateName = selectBtn.innerText;
    ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 200) {
            const thisTemplate = JSON.parse(ajax.responseText);
            location.href = "/update-delete-template/" + thisTemplate[0].id;
        }
    };
    ajax.open("GET", "/search-template?search_string=" + getTemplateName);
    ajax.send();
});

ajax.open("GET", "/templates-list");
ajax.send();
