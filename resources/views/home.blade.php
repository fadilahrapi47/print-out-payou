@extends('layouts.main')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 w-100 mt-5 mb-2">
        <div class="d-block py-3 mb-1">
          <div class="d-flex">
            <a href="/create-new-template" class="btn btn-success me-3">Create New Template</a>
            <div class="me-auto" style="width: 250px;">
              <!-- Custom Select with Search -->
              <div class="wrapper">
                <div class="select-btn">
                  <span>Cash Disbursement Half</span>
                  <i class="bi bi-caret-down-fill"></i>
                </div>
                <div class="content">
                  <div class="search">
                    <i class="bi bi-search"></i>
                    <input spellcheck="false" type="text" placeholder="Search" id="search-template">
                  </div>
                  <ul class="options"></ul>
                </div>
              </div>
              <!-- Custom Select with Search -->
            </div>
            <a class="btn btn-info me-2 text-white" id="update-btn">
              <i class="bi bi-pencil-fill" style="font-size: 18px;"></i>
            </a>
            <a href="#" class="btn btn-info ms-1 text-white dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="1,15">
              <i class="bi bi-share-fill" style="font-size: 18px;"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-end">
              <li><a class="dropdown-item" href="#"><i class="bi bi-printer-fill text-primary" style="padding-right: 10px;"></i>Export</a></li>
              <li><hr class="dropdown-divider"></li>
              <li><a class="dropdown-item" href="#"><i class="bi bi-file-earmark-pdf-fill text-danger" style="padding-right: 10px;"></i>Export To PDF</a></li>
              <li><hr class="dropdown-divider"></li>
              <li><a class="dropdown-item" href="#"><i class="bi bi-link-45deg text-primary fs-5" style="padding-right: 7px;"></i>Export Link</a></li>
            </ul>
          </div>
        </div>
        <div class="position-relative d-block bg-white shadow" id="template-content">
          {!! $templates[0]->template_content !!}
        </div>
      </div>
    </div>
  </div>
  <script src="{{ asset('js/app.js') }}"></script>
@endsection
