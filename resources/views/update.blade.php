@extends('layouts.main')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 w-100 mt-5 mb-2">
        <div class="d-block py-3 mb-1">
          <div class="d-flex">
            <button type="button" class="btn btn-outline-danger me-2" data-bs-toggle="modal" data-bs-target="#backtohome">
              Back to Home
            </button>
            <div class="modal fade" id="backtohome" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 style="font-weight: bold; color: black">Are you sure?</h1>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">No</button>
                    <a href="/" class="btn btn-success">Yes </a>
                  </div>
                </div>
              </div>
            </div>
            <form action="{{ route('delete') }}" id="deleteForm" method="POST">
              @csrf
              <input type="hidden" name="id_delete" value="{{ $template->id }}">
            </form>
            <button type="button" class="btn btn-outline-danger me-2" data-bs-toggle="modal" data-bs-target="#deletethis">
              Delete this Template
            </button>
            <div class="modal fade" id="deletethis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 style="font-weight: bold; color: black">Are you sure?</h1>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-success" form="deleteForm">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-success me-auto" data-bs-toggle="modal" data-bs-target="#saveTemplate">Update Template</button>
            <div class="modal fade" id="saveTemplate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Update New Template</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form class="form-floating" id="updateForm" method="POST" action="{{ route('update') }}">
                      @csrf
                      <input type="hidden" name="id_update" value="{{ $template->id }}">
                      <input type="text" class="form-control" name="title" value="{{ $template->template_name }}">
                      <label for="floatingInputValue">Template Name</label>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Close</button>
                    <button type="submit" name="submit" form="updateForm" class="btn btn-success">Update Template</button>
                  </div>
                </div>
              </div>
            </div>
            {{-- <div class="ms-auto px-5 d-none" id="templateid">Test</div> --}}
          </div>
        </div>
          <!-- TinyMCE Text Editor Area -->
          <div class="d-block bg-white">
            <textarea type="text" id="tinymceditor" name="body" form="updateForm">
              {!! $template->template_content !!}
            </textarea>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    const inputForm = document.querySelector('input[name="title"]');
    const btnSubmit = document.querySelector('button[name="submit"]');
    inputForm.addEventListener("keyup", (e) => {
      if (e.target.value == "") {
        btnSubmit.setAttribute("disabled", "");
      } else {
        btnSubmit.removeAttribute("disabled");
      }
    });
  </script>
@endsection
