<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Template;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function templates()
    {
        $templates = Template::all();
        return view('home', compact('templates'));
    }

    public function templatesList()
    {
        $templates = Template::all();
        return $templates;
    }

    public function searchTemplate(Request $request)
    {
        $templates = Template::where('template_name', 'like', '%'.$request->search_string.'%')->get();

        if ($templates->count() >= 1) {
            return $templates;
        } else {
            return response()->json([
                'status' => 'nothing_found'
            ]);
        }
    }

    public function store(Request $request)
    {
        $data = new Template;
        $data->template_name = $request->title;
        $data->template_content = $request->body;
        $data->save();

        return redirect('/');
    }

    public function update(Request $request)
    {
        $data = Template::find($request->id_update);
        $data->template_name = $request->title;
        $data->template_content = $request->body;
        $data->update();

        return redirect('/');
    }

    public function delete(Request $request)
    {
        $data = Template::find($request->id_delete);
        $data->delete();

        return redirect('/');
    }

}
