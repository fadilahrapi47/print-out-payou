<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Print Out Payou</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link rel="icon" href="https://payou.asia/img/payoulogo.png" type="image/x-icon">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;0,1000;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900;1,1000&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Custom CSS Style -->
        <style>
            .dropdown-toggle::after {
                content: none;
            }
            .btn {
                font-family: 'Mulish', sans-serif;
                font-weight: bold;
                text-align: center;
                display: flex;
                align-items: center;
                overflow: hidden;
                position: relative;
                z-index: 1;
                border-radius: 5px;
            }
            .btn-success {
                background-color: #20BF6B;
                border: none;
            }
            .btn-success:hover {
                background-color: rgb(25, 152, 85);
            }
            .dropdown-menu {
                border-radius: 0;
                font-size: 14px;
            }
            .dropdown-divider {
                margin: 0;
            }
        </style>
    </head>

    <body class="bg-light">
        <main class="container px-0 pb-5">
            @yield('content')
        </main>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>

        <!-- TinyMCE Text Editor Script -->
        <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
        <script>
            tinymce.init({
                selector: 'textarea#tinymceditor', // Replace this CSS selector to match the placeholder element for TinyMCE
                plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons accordion editimage',
                branding: false,
                promotion: false,
                height: 600,
                menu: {
                    file: { title: 'File', items: 'newdocument restoredraft | preview | export print | deleteallconversations' },
                    edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace' },
                    view: { title: 'View', items: 'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen | showcomments' },
                    insert: { title: 'Insert', items: 'image link media addcomment pageembed template codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor tableofcontents | insertdatetime' },
                    format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript codeformat | styles blocks fontfamily fontsize align lineheight | forecolor backcolor | language | removeformat' },
                    tools: { title: 'Tools', items: 'spellchecker spellcheckerlanguage | a11ycheck code wordcount' },
                    table: { title: 'Table', items: 'inserttable | cell row column | advtablesort | tableprops deletetable' },
                    help: { title: 'Help', items: 'help' }
                },
                help_tabs: [
                    'shortcuts',
                    {
                        name: 'petunujuk',
                        title: 'petunjuk penggunaan',
                        items: [
                            {
                                type: 'htmlpanel',
                                html: '<p>ini adalah halaman petunjuk penggunaan fitur edit custom template untuk user</p>'
                            }
                        ]
                    },
                ],
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | align numlist bullist | link image | accordion accordionremove | table media | lineheight outdent indent | code fullscreen preview | forecolor backcolor removeformat | charmap emoticons | save print | pagebreak anchor codesample | ltr rtl',
                quickbars_selection_toolbar: 'bold italic underline | fontfamily fontsize | blockquote quicklink',
                quickbars_insert_toolbar: 'quicktable quickimage quicklink hr pagebreak',
                quickbars_image_toolbar: 'alignleft aligncenter alignright',
                contextmenu: 'link image table',
                toolbar_sticky: true,
                toolbar_mode: 'sliding',
                image_advtab: true,
                importcss_append: true,
                image_caption: true,
                noneditable_class: 'mceNonEditable',
                autosave_ask_before_unload: true,
                autosave_interval: '30s',
                autosave_prefix: '{path}{query}-{id}-',
                autosave_restore_when_empty: true,
                autosave_retention: '30m',
                templates: [
                    { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
                    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
                    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
                ],
                // template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
                // template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
                file_picker_callback: function (cb, value, meta) {
                    const input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    
                    // add event listeners
                    input.addEventListener('change', (e) => {
                        const file = e.target.files[0];
                        const reader = new FileReader();
                        reader.addEventListener('load', () => {
                            // handle data processing with a blob
                            const id = 'blobid' + (new Date()).getTime();
                            const blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                            const base64 = reader.result.split(',')[1];
                            const blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            // add a callback and get the file name
                            cb(blobInfo.blobUri(), { title: file.name });
                            });
                        reader.readAsDataURL(file);
                    });
                    input.click();
                },
            });
        </script>
    </body>
</html>
